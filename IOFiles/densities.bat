:: Short (batch) script to output densities of testcases.
:: Can be extended to output certain information about cases
:: (like size, edge count, density, etc.).
@echo off
@setlocal enabledelayedexpansion
for /f "tokens=1,2,3" %%a in (densities.txt) do echo =(%%c/(%%a*%%b) ) >>densities.txt

pause