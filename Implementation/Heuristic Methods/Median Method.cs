﻿namespace HeuristicMethods;

using Datastructures;

public class Median_Method
{
    // Implementation of the Median Heuristic method.
    public static IEnumerable<Tuple<int, int, Node>> MedianHeuristicMethod(Graph G)
    {
        /*
         * Plan:
         * Datastructures for:
         *   - Nodes
         *   - Side of nodes
         *   - Edges (dictionary stored per node? like Dictionary<Node, Neighbours>)
         *
         *  Assumption structure:
         *      - Nodes: simple integers seperated by spaces (i.e. 1 2 3 4 5 )
         *      - Edges: tuples of pairs of valid nodes seperated by spaces WITHOUT spaces inside the tuples (i.e. (1,2) (2,3)
         *          also: assume first is in L, second in U.
         *
         *
         *  Implementation:
         *  Given Graph G = (L, U, E), with fixed order sigma_L on L, find a order for U such that crossings is minimal.
         *  Median is given by:
         *    for node u in U:
         *      N_u = { v_1, v_2, ..., v_k} where v_i in L neighbours of u (i.e. (u,v_i) in E)
         *        (note: assume v_i are ordered as in sigma_L (i.e. if i < j then sigma_L(v_i) < sigma_L(v_j) )
         *        (also: x_0(v) is the function that returns the x-coordinate of v given the ordering)
         *      med(u) = x_0(v_{floor(k)})
         */


        // Using the neighbours of a node u, the median is the middle of all neighbours of u (under the assumption that
        // the neighbours are sorted. This is a valid assumption given the structure of the edges (they are given in
        // ascending order.))
        // If no neighbours, the median is node '-1'. If two have the same median, they are sorted in arbitrary order
        // based on degree (odd before even). 
        Tuple<int, int, Node>[] Medians = new Tuple<int, int, Node>[G.U];

        for (int i = 0; i < G.U; i++)
        {
            int sz = G.NeighboursU[i].Count;
            if (sz > 1)
            {
                Medians[i] =
                    new Tuple<int, int, Node>(G.NeighboursU[i][(int)Math.Floor((double)sz / 2) - 1].order, -sz % 2,
                        G.Nodes[G.L + i]);
            }
            else
            {
                Medians[i] =
                    new Tuple<int, int, Node>(sz == 0 ? -1 : 0, -sz % 2,
                        G.Nodes[G.L + i]);
            }
        }

        // The medians are sorted, as mentioned above, in order of median, degree and finally the node itself
        // (default node order as given at the start, 0 to |L + |U|) in ascending order.
        IEnumerable<Tuple<int, int, Node>> sortedMedians = 
            Medians.OrderBy(y => y.Item1)
                .ThenBy(y => y.Item2)
                .ThenBy(y => y.Item3.order);

        return sortedMedians;
    }
    
   
    
}
