﻿namespace HeuristicMethods;

using Datastructures;

public class Barycenter_Method
{
    public static IEnumerable<Tuple<double, int, Node>> BarycenterHeuristicMethod(Graph G)
    {
        /*
         * Similar to median, except the assigned position is the average order of its neighbours,
         * rather than the order of the middle neighbour.
         */


        // Using the neighbours of a node u, the median is the average order of all neighbours of u.
        
        
        // If no neighbours, the median is node '-1'. If two have the same barycenter, they are kept in the same order
        // they started (node order between |L| and |L| + |U|).
        Tuple<double, int, Node>[] Barycenters = new Tuple<double, int, Node>[G.U];

        for (int i = 0; i < G.U; i++)
        {
            List<Node> nbs = G.NeighboursU[i];
            int sz = G.NeighboursU[i].Count;
            Barycenters[i] =
                new Tuple<double, int, Node>(sz == 0 ? 0.0 : nbs.Average(x => x.order), 0, G.Nodes[G.L + i]);
        }

        // The medians are sorted, as mentioned above, in order of median, degree and finally the node itself
        // (default node order as given at the start, 0 to |L + |U|) in ascending order.
        IEnumerable<Tuple<double, int, Node>> SortedBarycenters = 
            Barycenters.OrderBy(y => y.Item1)
                .ThenBy(y => y.Item2)
                .ThenBy(y => y.Item3.order);
        

        return SortedBarycenters;
    }


}