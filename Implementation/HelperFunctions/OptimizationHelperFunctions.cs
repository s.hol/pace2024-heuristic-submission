﻿using System.Collections;
using System.Data;

namespace HelperFunctions;

using Datastructures;

// This class contains helperfunctions that are used for optimizing the found solution, and their dependencies.
// These functions are applied after the general method is tried, and return their best result for that specific method.

public static class OptimizationHelperFunctions
{
    
    // Two helperfunctions to (easier) retrieve barycenter/median of a node.
    public static double GetBarycenter(Node x, Graph G)
    {
        return G.NeighboursU[x.order - G.L - 1].DefaultIfEmpty(new Node("-1")).Average(y => y.order);
    }
    
    public static int GetMedian(Node x, Graph G)
    {
        return G.NeighboursU[x.order-G.L][(int)Math.Floor((double)G.NeighboursU[x.order-G.L].Count / 2) - 1].order;
    }
    
    
    
    
    // Function to get best perutation. Based on the function from:
    // https://codereview.stackexchange.com/questions/226804/linq-for-generating-all-possible-permutations
    
    // This function is for finding all permutations. 
    // Basic structure is:
    // Recursively add all remaining options to the "prefix". If no remaining options, return prefix. 
    public static IEnumerable<T[]> Permute<T>(this IEnumerable<T> source)
    {
        return permute(source, Enumerable.Empty<T>());
        IEnumerable<T[]> permute(IEnumerable<T> remainder, IEnumerable<T> prefix) =>
            !(remainder.Count() > 0) ? new[] { prefix.ToArray() } :
                remainder.SelectMany((c, i) => permute(
                    remainder.Take(i).Concat(remainder.Skip(i+1)).ToArray(),
                    prefix.Append(c)));
    }
    
    
    // Function above modified to store only the permutation with the least amount of crossings.
    // Used for the "Optimal" tiebreaking method. Not used due to crossing count of all permutations being too slow.
    public static IEnumerable<Node> ReturnBestPermute(this IEnumerable<Node> source, Graph G)
    { 
        (IEnumerable<Node>, ulong) acc = new (Enumerable.Empty<Node>(), ulong.MaxValue);
        return permute(source, Enumerable.Empty<Node>());

        IEnumerable<Node> permute(IEnumerable<Node> remainder, IEnumerable<Node> prefix)
        {
            while (acc.Item2 > 0)
            {
                if (!(remainder.Count() > 0))
                {
                    if (prefix.Count() != source.Count())
                        throw new ArgumentException($"Result {prefix.Count()} is not of correct length {source.Count()}.");
                    
                    ulong count = PartialOrderCrossingCount(prefix.ToArray(), G, acc.Item2);
                    acc = count < acc.Item2 ? (prefix, count) : acc;
                    return acc.Item1;
                }

                return remainder.SelectMany((c, i) => permute(
                    remainder.Take(i).Concat(remainder.Skip(i + 1)),
                    prefix.Append(c)));
            }

            return acc.Item1;
        }
    }
    
    
    // Function from:
    // https://stackoverflow.com/questions/64955703/how-to-get-a-cartesian-product-of-a-list-containing-lists
    // And the linked post:
    // https://stackoverflow.com/questions/3093622/generating-all-possible-combinations/3098381#3098381
    
    // Function gives the cartesian product of lists, i.e. all combinations from all lists.
    // Example: [1,2,3] x [4,5] x [6,7] => [[1,4,6], [1,4,7], [1,5,6], [1,5,7], ... ]
    
    // Used for the original plan of checking all possible permutations of a vertex set.
    // Idea scrapped because performance issues with the method.
    public static IEnumerable<IEnumerable<T>> CartesianProduct<T>(this IEnumerable<IEnumerable<T>> sequences)
    {
        IEnumerable<IEnumerable<T>> emptyProduct = new[] { Enumerable.Empty<T>()};
        return sequences.Aggregate(
            emptyProduct,
            (accumulator, sequence) =>
                from accseq in accumulator
                from item in sequence
                select accseq.Concat(new[] {item})
        );
    }
    
    // -----
    // Helperfunctions for applying different tie-breaking implementations.

    public static Node[] ReverseBarycenter(this IEnumerable<IEnumerable<Node>> groupedList, Graph G)
    {
        IEnumerable<Node> result = Enumerable.Empty<Node>();
        foreach (IEnumerable<Node> order in groupedList)
        {
            IEnumerable<Node> temp = order.OrderByDescending(x => GetBarycenter(x, G));
            result = result.Concat(temp);
        }

        if (result.Count() != G.U)
            throw new ArgumentException($"The result is not of the correct length: {result.Count()} instead of {G.U}.");
            
        return result.ToArray();
    }
    
    public static Node[] Reverse(this IEnumerable<IEnumerable<Node>> groupedList, Graph G)
    {
        IEnumerable<Node> result = Enumerable.Empty<Node>();
        foreach(IEnumerable<Node> order in groupedList)
            result = result.Concat(order.Reverse());
        
        if (result.Count() != G.U)
            throw new ArgumentException($"The result is not of the correct length: {result.Count()} instead of {G.U}.");
        
        return result.ToArray();
    }

    public static Node[] Random(IEnumerable<IEnumerable<Node>> groupedList, Graph G)
    {
        // Apply a method called Fisher-Yates shuffle.
        // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
        // (Basically just swapping each elements with another, at 'random')
        
        Random random = new Random();
        IEnumerable<Node> result = Enumerable.Empty<Node>();
        foreach (IEnumerable<Node> order in groupedList)
        {
            Node[] elements = order.ToArray();
            for (int i = order.Count() - 1; i > 0; i--)
            {
                int swapIndex = random.Next(i + 1);
                (elements[i], elements[swapIndex]) = (elements[swapIndex], elements[i]);
            }
            result = result.Concat(elements);
        }

        if (result.Count() != G.U)
            throw new ArgumentException($"The result is not of the correct length: {result.Count()} instead of {G.U}.");
        
        return result.ToArray();
    }

    public static Node[] ReverseOptimal(this IEnumerable<IEnumerable<Node>> groupedList, Graph G)
    {
        /*
            Idea of reverse optimal is to minimise the crossings between edges adjacent to tie-breaking vertices.
            So take the grouped lists, and per group (grouped by median/barycenter) do the following:
            Take all permutations of the group and loop over them to find the best (in terms of crossingscounter).
        */
        
        IEnumerable<Node> result = groupedList.Aggregate(Enumerable.Empty<Node>(),
            (acc, x) => acc.Concat(FindOptimalPermutation(x,G)));    
        if (result.Count() != G.U)
            throw new ArgumentException($"The result is not of the correct length: {result.Count()} instead of {G.U}.");
        
        return result.ToArray();
    }


    // Method to calculate the number of crossings for a partial order.
    // Only count crossings between vertices in this partial order.
    private static ulong PartialOrderCrossingCount(Node[] partialOrder, Graph G, ulong limit)
    {
        if (limit == 0)
            return ulong.MaxValue;  
        
        ulong numberOfCrossings = 0;
        List<Node> N_u, N_uprime;
        int l, u, result;

        for (int i = 0; i < partialOrder.Length; i++) // u
        {
            N_u = G.NeighboursU[partialOrder[i].order - G.L - 1];
            for (int k = i + 1; k < partialOrder.Length; k++) // u'
            {
                N_uprime = G.NeighboursU[partialOrder[k].order - G.L - 1];
                l = 1;
                u = N_uprime.Count;
                for (int j = 0; j < N_u.Count(); j++) // v
                {
                    if (numberOfCrossings >= limit)
                        return ulong.MaxValue;
                    result = SolutionHelperFunctions.BinaryCount(l, u,
                        N_u[j], // v
                        N_uprime); // all v'
                    l = result == 0 ? 1 : result;

                    numberOfCrossings += (ulong)result;

                }
            }
        }


        return numberOfCrossings;

    }

    // Method to determine which permutation of a segment is the best in terms of the number of crossings.
    private static IEnumerable<Node> FindOptimalPermutation( IEnumerable<Node> segment, Graph G)
    {
        IEnumerable<Node> res = segment.ReturnBestPermute(G);
        return res;
    }
}