﻿using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using System.Numerics;
using System.Runtime.InteropServices.ComTypes;

namespace HelperFunctions;

using Datastructures;

// This class contains helperfunctions dedicated to mostly debugging:
// E.g. crossingscalculation, output to console, compare with existing solutionfile, etc.

public class SolutionHelperFunctions
{
    // Method to calculate number of crossings of a given order, using standard formula.
    // (for each edge (u,v), sum all edges with 'start' left of u, and 'end' right of v) 
    public static ulong CrossingCounter(Node[] foundOrder, Graph G)
    {
        ulong numberOfCrossings = 0;
        
        // Since the neighbours are sorted, it is enough when we find the first that satisfies the condition,
        // and all the remaining nodes (with a larger order) must satisfy the condition  as well.
        // (assuming, of course, iteration from left to right)
        List<Node> N_u, N_uprime;
        int l, u, result;
        for (int i = 0; i < foundOrder.Length; i++) // u
        {
            N_u = G.NeighboursU[foundOrder[i].order - G.L - 1];
            for (int k = i + 1; k < foundOrder.Length; k++) // u'
            {
                N_uprime = G.NeighboursU[foundOrder[k].order - G.L - 1];
                l = 1;
                u = N_uprime.Count;
                for (int j = 0; j < N_u.Count(); j++) // v
                {
                    result = BinaryCount(l, u,
                        N_u[j], // v
                        N_uprime); // all v'
                    l = result == 0 ? 1 : result;

                    numberOfCrossings += (ulong)result;

                }
            }
        }
        return numberOfCrossings;
    }

    // A method that checks if the found order is correct, based on a specified solutionFile.
    public static bool SolutionChecker(Node[] foundOrder, string solutionFile)
    {
        bool correctness = true;
        using (StreamReader reader = new StreamReader(solutionFile))
        {
            while (correctness)
                for (int i = 0; i < foundOrder.Length; i++)
                    correctness |= reader.ReadLine() == foundOrder[i].ToString();
        }

        return correctness;
    }

    // Method to write found order to a specified file.
    // Mostly debugging, and in order to (possibly) make own testcases.
    public static void OutputOrderFile(Node[] foundOrder, string outputFile)
    {
        File.WriteAllLines(outputFile, foundOrder.Select(x => x.ToString()));
    }

    // Small method that write the found order to console.
    // Mostly debugging purposes, to see if found order is indeed correct.
    public static void OutputOrderConsole(Node[] foundOrder)
    {
        for (int i = 0; i < foundOrder.Length-1; i++)
            Console.WriteLine(foundOrder[i].order);
        Console.Write(foundOrder[foundOrder.Length-1].order);
    }
    
    // Way to count number of crossings by using a form of binary search:
    // We know that the neighbours of a node are sorted in ascending order, hence,
    // the moment we find the first node that satisfy the crossingscondition
    // (default definition: (u,v). (u', v') cross <=> u.order < u'.order && v.order > v'.order
    // (or vice versa, but we need to check only one, otherwise we count crossings double)
    // So, since nodes are sorted in ascending order, if we find u' \in N_v' crosses with (u,v), then all nodes before/after u' too.
    // Conclusion: find first element that is bigger then the input node, and we can return the crossingscount
    // by returning l or u (already compensated for the 0-indexbased-ness).
    // (if we chose u' > threshold, we would get |N_v'| - indexOf(u') )
    public static int BinaryCount(int l, int u, Node threshold, List<Node> order)
    {
        // l is lower bound, u upper bound, threshold the value to check against.
        // If segment length is one, it either matches or not. Determine result based on that.
        if (l == u)
            return order[l-1].order < threshold.order ? l : 0;
        if(u - l == 1)
            return order[l-1].order < threshold.order ?  order[u-1].order < threshold.order ? u : l : 0;

        // Take midpoint of segment, and check if it is larger of smaller then the threshold.
        // If m is larger (or equal), the smallest index (if it exists) that satisfies is in the left half of the remaining segment.
        // If m is smaller, then either it is the first, or the largest is in the right half.
        int m = (l + u) / 2;
        
        if (order[m].order < threshold.order)
            return BinaryCount(m, u, threshold, order);

        return BinaryCount(l,m,threshold,order);
    }

    
    // Sweepline algorithm implementation.
    // Stopped because we wouldn't be able to finish the implementation in time.
    /*
    private static ulong SweepLineCount(Graph G, List<Node> order)
    {
        // Source:
        // https://www.educative.io/blog/sweep-line-algorithm-line-segment-intersection
        
        // Note that L (fixed side) is sorted, and that the order of G.U is determined (the given order),
        // so we can insert it directly into the queue without additional sorting.
        
        // Besides that, the queue will contain all nodes, and an unspecified number of intersections. 
        ulong crossingsCount = 0;
        Queue<Node> eventQueue = new Queue<Node>();
        OrderedDictionary<Node> sweepLineStatus = new OrderedDictionary<Node>();
        
        for(int i = 0; i < G.L; i++)
            eventQueue.Enqueue(G.Nodes[i]);
        for(int i = 0; i < G.U; i++)
            eventQueue.Enqueue(order[i]);
        
        // The events points for the algorithm are all nodes of G, so we can loop over our event points as follows:
        while(eventQueue.Count > 0)
        {
            Node point = eventQueue.Dequeue();
            if (point.order < G.L) // This means it is a node in L, hence considered an endpoint.
            {
                // We have to do three things:
                // 1. Insert the point on the sweepLineStatus, and find (if any) the two line segments q and t,
                // above and below s respectively. 
                // 2. Remove (if applicable) intersection between q and t from queue.
                // 3. Check for intersection between q and s & q and t, and add to queue if there are any.
                
                Node above = null, below = null;
                sweepLineStatus.Add(point, point);
            }
            else if (point.order > G.L) // Then the point is in G.U, hence a starting point of a new edge/line segment.
            {
                    
            }
        }
        
        return crossingsCount;
    }
    */
    
}