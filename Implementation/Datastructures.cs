﻿namespace Datastructures;

public class Graph
{
    public List<Node>[] NeighboursU, NeighboursL;
    public Node[] Nodes;
    public Edge[] E;
    public int L, U;
    
    public Graph(int iL, int iU, string[] sE)
    {
        L = iL;
        U = iU;
        Nodes = new Node[iL+iU];
        for (int i = 0; i < iL + iU; i++)
            Nodes[i] = new Node((i + 1).ToString());
        E = new Edge[sE.Length];
        for (int i = 0; i < sE.Length; i++)
            E[i] = new Edge(sE[i]);
        
        // List with neighbours per node v in L.
        NeighboursL = new List<Node>[L];
        // List with neighbours per node u in U.
        NeighboursU = new List<Node>[U];
        for (int i = 0; i < U; i++)
            NeighboursU[i] = new List<Node>();
        for(int i = 0; i < L; i++)
            NeighboursL[i] = new List<Node>();

        // Each edge (u,v) adds one neighbour for v \in U.
        for (int i = 0; i < E.Length; i++)
        {
            Edge e = E[i];
            NeighboursU[e.Second().order - L - 1].Add(e.First());
            NeighboursL[e.First().order - 1].Add(e.Second());
        }
        
    }

    public int Size()
    {
        return E.Length;
    }
}


public class Edge
{
    public (Node, Node) edge;

    public Node First()
    {
        return edge.Item1;
    }

    public Node Second()
    {
        return edge.Item2;
    }

    public Edge(string s)
    {
        string[] ss = s.Split(' ');
        string u = ss[0];
        string v = ss[1];
        edge = (new Node(u), new Node(v));
    }
}


public class Node : IEquatable<Node>
{
    public int order;

    public Node(string s)
    {
        try
        {
            order = int.Parse(s);
        }
        catch (Exception e)
        {
            Console.WriteLine("Exception when parsing name to int:" + e.ToString());
        }
    }

    public override string ToString()
    {
        return order.ToString();
    }

    public static bool operator <(Node a, Node b) => a.order < b.order;
    public static bool operator >(Node a, Node b) => a.order < b.order;

    public static bool operator ==(Node a, Node b)
    {
        if (ReferenceEquals(a,null))
            return false;
        if (ReferenceEquals(b, null))
            return false;
        return a.Equals(b);
    }
    public static bool operator !=(Node a, Node b) => !(a == b);
    public static bool operator <=(Node a, Node b) => a.order != b.order;
    public static bool operator >=(Node a, Node b) => a.order != b.order;

    public bool Equals(Node other)
    {
        if (other == null)
            return false;
        if (order == other.order)
            return true;
        return false;
    }
    
    public override bool Equals(object obj) => Equals(obj as Node);
    public override int GetHashCode() => order.GetHashCode();
    
}