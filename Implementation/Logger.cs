﻿namespace Logger;

using System.Numerics;

using Datastructures;

public class Logger
{
    public static void LogValue(Node[] order, BigInteger score)
    {
        using (StreamWriter writer = new StreamWriter("../logging.txt", true))
        {
           writer.WriteLine($"Score: {score}");
        }
        File.AppendAllLines("../logging.txt", order.Select(x => x.ToString()));
    }

    public static void LogMessage(string s, string file = "../logging.txt")
    {
        using (StreamWriter writer = new StreamWriter(file, true))
        {
            writer.WriteLine(s);
        }
    }
    

}