﻿namespace Parsers;

using Program;
using System.IO;
using Datastructures;

public class PACEParser
{
    public static Graph PACEFileParse(string fileName)
    {
        // Parser for dataformat as read in console, based on https://pacechallenge.org/2024/io/.
        // For detailed information, please refer to the link above.
        Graph G;
        List<string> edges = new List<string>();
        StreamReader reader = new StreamReader(fileName);
        string? s;
        string ocr;
        int n0 = 0, n1 = 0, m = 0;
        while ((s=reader.ReadLine()) != null)
        {
            if (!s.StartsWith("\n") && !s.StartsWith("c") && s.StartsWith("p"))
            {
                string[] args = s.Split(' ');
                ocr = args[1];
                n0 = int.Parse(args[2]);
                n1 = int.Parse(args[3]);
                m = int.Parse(args[4]);
            }
            else
            {
                edges.Add(s);
            }
        }

        if (edges.Count != m)
            Console.WriteLine("c Error: edge list not correct length.");
        G = new Graph(n0, n1, edges.ToArray());
        return G;
    }
    public static Graph PACEConsoleParse()
    {
        StreamReader reader = new StreamReader(Console.OpenStandardInput());
        Graph G;
        List<string> edges = new List<string>();
        string? s;
        string ocr;
        int n0 = 0, n1 = 0, m = 1;
        while ((s = reader.ReadLine()) != null && edges.Count < m)
        {
            if (!s.StartsWith("\n") && !s.StartsWith("c") && s.StartsWith("p"))
            {
                string[] args = s.Split(' ');
                ocr = args[1];
                n0 = int.Parse(args[2]);
                n1 = int.Parse(args[3]);
                m = int.Parse(args[4]);
            }
            else
            {
                edges.Add(s);
            }
        }
        if (edges.Count != m)
            throw new ArgumentException("c Error: edge list not correct length.");
        G = new Graph(n0, n1, edges.ToArray());
        return G;
    }
}