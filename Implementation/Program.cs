﻿namespace Program;

using System.Linq;
using System.Numerics;

using Parsers;
using Datastructures;
using HelperFunctions;
using HeuristicMethods;


public class Program
{
    // Method describing program-exits in C# from:
    // https://stackoverflow.com/questions/203246/how-can-i-keep-a-console-open-until-cancelkeypress-event-is-fired
    static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
    {
        SolutionHelperFunctions.OutputOrderConsole(foundOrder);
        SolutionHelperFunctions.OutputOrderFile(foundOrder, outputFile);
        
        keepRunning = false;
    }

    static void Console_Exit(object sender, EventArgs e)
    {
        keepRunning = false;

    }
    
    // -----
    private static volatile bool keepRunning = true;
    private static Node[] foundOrder;

    private static string outputFile;
    private static Graph G;

    static void Main(string[] args)
    {
        // First, register handler for sigterm signal:
        Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);
        AppDomain.CurrentDomain.ProcessExit += new EventHandler(Console_Exit);

        
        if(args == null || args.Length == 0)
            G = PACEParser.PACEConsoleParse();
        else
        {
            // Set basic variables for solution storage/in-/output.
            string inputFile = args[0];
            string outputPath = args[1];


            string inputFileName = inputFile.Split('/')[inputFile.Split('/').Length - 1].Split('.')[0];
            outputFile = outputPath;
            string outputFileSplit = outputPath.Split(".ans")[0];

            G = PACEParser.PACEFileParse(inputFile);
        }

        IEnumerable<Tuple<int, int, Node>> initialMedianOrder = Median_Method.MedianHeuristicMethod(G);
        IEnumerable<Tuple<double, int, Node>> initialBarycenterOrder = Barycenter_Method.BarycenterHeuristicMethod(G);

        IEnumerable<IEnumerable<Node>> groupedMedianOrder =
            from item in initialMedianOrder
            group item.Item3 by new { item.Item1, item.Item2 }
            into grouped
            select grouped;


        IEnumerable<IEnumerable<Node>> groupedBarycenterOrder =
            from item in initialBarycenterOrder
            group item.Item3 by new { item.Item1, item.Item2 }
            into grouped
            select grouped;


        while (keepRunning)
        {
            ulong score = 0, best = ulong.MaxValue;
            Node[] order;
            
            order = initialMedianOrder.Select(x => x.Item3).ToArray();
            score = SolutionHelperFunctions.CrossingCounter(order, G);

            if (score < best)
            {
                foundOrder = order;
                best = score;
            }

            order = initialBarycenterOrder.Select(x => x.Item3).ToArray();
            score = SolutionHelperFunctions.CrossingCounter(order, G);
            if (score < best)
            {
                foundOrder = order;
                best = score;
            }

            order = Optimize<IEnumerable<Node>>(groupedBarycenterOrder, G, OptimizationHelperFunctions.Random);
            score = SolutionHelperFunctions.CrossingCounter(order, G);
            if (score < best)
            {
                foundOrder = order;
                best = score;
            }

            order = Optimize<IEnumerable<Node>>(groupedBarycenterOrder, G, OptimizationHelperFunctions.Reverse);
            score = SolutionHelperFunctions.CrossingCounter(order, G);
            if (score < best)
            {
                foundOrder = order;
                best = score;
            }


            // Disable reverse optimal due to performance issues on calculating the permutations.

            // order = Optimize(initialBarycenterOrder, G, OptimizationHelperFunctions.ReverseOptimal);
            // score = SolutionHelperFunctions.CrossingCounter(order, G);
            // if (score < best)
            // {
            //     foundOrder =  initialBarycenterOrder.Select(x => x.Item3).ToArray();
            //     best = score;
            // }

            order = Optimize<IEnumerable<Node>>(groupedMedianOrder, G,
                OptimizationHelperFunctions.ReverseBarycenter);
            score = SolutionHelperFunctions.CrossingCounter(order, G);
            if (score < best)
            {
                foundOrder = order;
                best = score;
            }

            // After running all methods, break the while-loop.
            // If some form of iterative improvement is used, remove this.
            break;
        }

        if (foundOrder.Length != G.U)
            throw new ArgumentException("The result is not of the correct length: " + foundOrder.Length +
                                        " instead of " + G.U + ".");
        if(args == null || args.Length == 0)
            SolutionHelperFunctions.OutputOrderConsole(foundOrder);
        else
            SolutionHelperFunctions.OutputOrderFile(foundOrder, outputFile);
    }


    public static Node[] Optimize<T>(IEnumerable<IEnumerable<Node>> groupedList, Graph G, Func<IEnumerable<IEnumerable<Node>>, Graph, Node[]> f)
    {
        /*
            Now we want to apply different tie-breaking methods.
            For now, we want:
                - Median ties:
                    * Reverse Barycenter
                - Barycenter ties:
                    * Reverse
                    * Random
                    * Reverse optimal (Dropped due to time/performance--issues

         */
        return f(groupedList, G);
    }
    
    // Not used since it is not fast enough with the current crossing counter.
    private static Node[] FindOptimalPermutation<T>(IEnumerable<Tuple<T, int, Node>> initialOrder)
    {
        // Permute the initial order to see if permutations are better or not.
        // (better := less total crossings)

        // For each set in the initial order (a set is grouped by appointed order and degree)*,
        // take all possible permutations of that set.

        // * This is supposed to be all "equivalent" nodes in the median heuristic, which normally get an arbitrary order.
        // This is meant to see if other orders (that are supposed to be equivalent), make a difference.  


        // Grouping:
        // [ [(1),(2),(3)] ] => [ [1,2], [3] ] =>
        // Followed by groupwise Permutations:
        // [ [ [1,2], [2,1] ], [ [3] ] ] =>

        IEnumerable<IEnumerable<Node[]>> permutableLists =
            from item in initialOrder
            group item.Item3 by new { item.Item1, item.Item2 }
            into grouped
            select grouped.Permute();
        
        
        // Cartesian product of the lists of permutations. 
        // [ [1,2, 3], [2,1,3] ]
        // Then, find optimal permutation by iterating all permutations and check if they are better crossingswise.
        Node[] opt = new Node[G.U];
        IEnumerable<Node[]> temp = permutableLists.CartesianProduct().Select(y => 
            y.Aggregate(new Node[]{}, (acc, x) => acc.Concat(x).ToArray() ));
        
        opt = temp.Aggregate(initialOrder.Select(y => y.Item3).ToArray(),
                    (acc, x) =>
                        SolutionHelperFunctions.CrossingCounter(acc, G) < SolutionHelperFunctions.CrossingCounter(x, G)
                            ? acc
                            : x)
                .ToArray();


        return opt;
    }
    
    
    
}
