﻿namespace GraphGenerators;

public class RandomGraphGenerator
{

        public static (int,int,string[]) GraphGenerator(string[] args)
        {
            // Let args contain: int LL, int UL, int LU, int UU, int d
            // First: (LL and UL) and (LU and UU) are lower and upper size-limit of L and U respectively.
            // Furthermore, d denotes the (desired) density of the graph we want to generate.
            // This is supposed to work basically like a percentage, so 0 <= d <= 100.
            
            // The idea is then to generate a graph with the parameters as follows:
            // 1. Take random integers in the given range to determine size of the sides.
            // 2. For each possible edge (u,v) with u in L and v in U, roll a percentage.
            // If p > d then it is not added to the graph, otherwise it is added as an edge.
            
            // This obviously is not a fast method, but it (on average) generates a graph within the desired parameters,
            // which is useful, and meant for, testing, without having to manually input and create graphs.
            int LL = int.Parse(args[1]);
            int UL = int.Parse(args[2]);
            int LU = int.Parse(args[3]);
            int UU = int.Parse(args[4]);
            int d = int.Parse(args[5]);
            Random rng = new Random();
            int L = rng.Next(LL,UL);
            int U = rng.Next(LU,UU);
            List<string> E = new List<string>();

            for(int i = 1; i <= L; i++)
                for (int j = 1; j <= U; j++)
                {
                    int p = rng.Next(101);
                    if(p <= d )
                        E.Add(i + "," + j);
                }

            return (L, U, E.ToArray());
        }
}