using Datastructures;
using HelperFunctions;
using Parsers;
using Program;


namespace Tests;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    
    // Test to assert if neighbours are indeed sorted.
    [Test]
    public void NeighboursSortedSmallSet()
    {
        Graph G;
        string path = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.Parent.FullName + "\\IOFiles\\SmallSet";
        foreach (var f in Directory.GetFiles(path, "*.gr"))
        {
            G = PACEParser.PACEFileParse(f);

            for (int i = 0; i < G.U; i++)
                for (int j = 0; j < G.NeighboursU[i].Count - 1; j++)
                    if (!(G.NeighboursU[i][j] <= G.NeighboursU[i][j + 1]))
                        Assert.Fail($"The neighbours of {f} were not ordered correctly.");
                
        }
        path = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.Parent.FullName + "\\IOFiles\\InputFiles";
        foreach (var f in Directory.GetFiles(path, "*.gr"))
        {
            G = PACEParser.PACEFileParse(f);

            for (int i = 0; i < G.U; i++)
                for (int j = 0; j < G.NeighboursU[i].Count - 1; j++)
                    if (!(G.NeighboursU[i][j] <= G.NeighboursU[i][j + 1]))
                        Assert.Fail($"The neighbours of {f} were not ordered correctly.");
                
        }
        Assert.IsTrue(true);
    }
    
    // Test (WIP) to see of the crossings counter does indeed give the correct answer for a given order.
    // Lacking: using a predetermined order with known number of crossings.
    [Test]
    public void TestCrossingsMethod()
    {
        // Test the method for counting the crossings on the small dataset provided
        // (the only one of which the solution is known) and check it gives a correct/reasonable answer.
        Graph G;
        string path = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.Parent.FullName + "\\IOFiles\\SmallSet";
        Dictionary<string, int> results = new Dictionary<string,int> {{"complete_4_5", 60}, {"cycle_8_shuffled",4},
            {"cycle_8_sorted",3}, {"grid_9_shuffled",17}, {"ladder_4_4_shuffled",11}, {"ladder_4_4_sorted",3},
            {"matching_4_4",0}, {"path_9_shuffled", 6}, {"path_9_sorted", 0}, {"plane_5_6",0}, {"star_6", 0},
            {"tree_6_10", 13}, {"website_20", 17} };
        foreach (var f in Directory.GetFiles(path, "*.gr"))
        {
            G = PACEParser.PACEFileParse(f);
            string problemName = f.Split('\\')[f.Split('\\').Length - 1].Split('.')[0];
            int ans = results[problemName];   
            for (int i = 0; i < G.U; i++)
            for (int j = 0; j < G.NeighboursU[i].Count - 1; j++)
            {
                IEnumerable<Tuple<int,int,Node>> res1 = HeuristicMethods.Median_Method.MedianHeuristicMethod(G);
                IEnumerable<Tuple<double,int,Node>> res2 = HeuristicMethods.Barycenter_Method.BarycenterHeuristicMethod(G);
                
                Node[] initialMedianOrder = res1.Select(x => x.Item3).ToArray();
                Node[] initialBarycenterOrder = res2.Select(x => x.Item3).ToArray();
                
                IEnumerable<IEnumerable<Node>> groupedMedianOrder =
                    from item in res1
                    group item.Item3 by new { item.Item1, item.Item2 }
                    into grouped
                    select grouped;


                IEnumerable<IEnumerable<Node>> groupedBarycenterOrder =
                    from item in res2
                    group item.Item3 by new { item.Item1, item.Item2 }
                    into grouped
                    select grouped;
                
                
                
                ulong score, best= ulong.MaxValue;
                score = SolutionHelperFunctions.CrossingCounter(initialMedianOrder, G);
                if (Math.Min(score,best) == (ulong) ans)
                    Assert.Fail($"The crossingscount is way to high for {f} with {score} compared to {ans}.");
                score = SolutionHelperFunctions.CrossingCounter(initialBarycenterOrder, G);
                if (Math.Min(score,best) == (ulong) ans)
                    Assert.Fail($"The crossingscount is way to high for {f} with {score} compared to {ans}.");
                score = SolutionHelperFunctions.CrossingCounter(
                    Program.Program.Optimize<IEnumerable<IEnumerable<Node>>>(groupedBarycenterOrder, G, OptimizationHelperFunctions.Random), G);
                if (Math.Min(score,best) == (ulong) ans)
                    Assert.Fail($"The crossingscount is way to high for {f} with {score} compared to {ans}.");
                score = SolutionHelperFunctions.CrossingCounter(
                    Program.Program.Optimize<IEnumerable<IEnumerable<Node>>>(groupedBarycenterOrder, G, OptimizationHelperFunctions.Reverse), G);
                if (Math.Min(score,best) == (ulong) ans)
                    Assert.Fail($"The crossingscount is way to high for {f} with {score} compared to {ans}.");
                score = SolutionHelperFunctions.CrossingCounter(
                    Program.Program.Optimize<IEnumerable<IEnumerable<Node>>>(groupedBarycenterOrder, G, OptimizationHelperFunctions.ReverseOptimal), G);
                if (Math.Min(score,best) == (ulong) ans)
                    Assert.Fail($"The crossingscount is way to high for {f} with {score} compared to {ans}.");
                score = SolutionHelperFunctions.CrossingCounter(
                    Program.Program.Optimize<IEnumerable<IEnumerable<Node>>>(groupedMedianOrder, G, OptimizationHelperFunctions.ReverseBarycenter ), G);
                if (Math.Min(score,best) == (ulong) ans)
                    Assert.Fail($"The crossingscount is way to high for {f} with {score} compared to {ans}.");
            }
        }
        Assert.IsTrue(true);
    }
}