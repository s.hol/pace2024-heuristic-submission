:: Short script to compare own crossings with the results from the pace2024verifier,
:: to test the crossingscounter function.
@echo off
SET "t=%~dp0%"
cd "Implementation"
SET "f=%~dp0%IOFiles\"
SET "p=%f%OutputFiles\"
>"%t%\output.txt" (
	FOR /r "%f%InputFiles\" %%i in (*.gr) DO (
		echo Next problem, %%~ni:
		dotnet run "%%i" "%p%%%~ni.ans" --property WarningLevel=0
		FOR /r "%p%\" %%j in (%%~ni.ans) DO (
			echo Method %%~nj:
			pace2024verifier -c --only-crossings "%%i" "%%j"
		)
		echo[
	)
)
PAUSE