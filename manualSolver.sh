#! bin/bash
# (shell) Script to run the solver on testcases at the specified location.
export f="$(pwd)"
cd /Implementation
for file in $f/IOFiles/InputFiles/*.gr
do
	echo Next problem: $file
	ff="$(basename ${file})"
	dotnet run "$file" "$f/OutputFiles/${ff%.gr}.ans" --property WarningLevel=0
	echo ""
done
