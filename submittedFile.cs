using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

public class Program
{
    // Method describing program-exits in C# from:
    // https://stackoverflow.com/questions/203246/how-can-i-keep-a-console-open-until-cancelkeypress-event-is-fired
    static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
    {
        SolutionHelperFunctions.OutputOrderConsole(foundOrder);
        keepRunning = false;
    }

    static void Console_Exit(object sender, EventArgs e)
    {
        keepRunning = false;
    }

    // -----
    private static volatile bool keepRunning = true;
    private static Node[] foundOrder;

    private static string outputFile;
    private static Graph G;

    static void Main()
    {
        // First, register handler for sigterm signal:
        Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);
        AppDomain.CurrentDomain.ProcessExit += new EventHandler(Console_Exit);
        
        G = PACEParser.PACEConsoleParse();

        
        IEnumerable<Tuple<int, int, Node>> initialMedianOrder = MedianMethod.MedianHeuristicMethod(G);
        IEnumerable<Tuple<double, int, Node>> initialBarycenterOrder = BarycenterMethod.BarycenterHeuristicMethod(G);

        IEnumerable<IEnumerable<Node>> groupedMedianOrder =
            from item in initialMedianOrder
            group item.Item3 by new { item.Item1, item.Item2 }
            into grouped
            select grouped;


        IEnumerable<IEnumerable<Node>> groupedBarycenterOrder =
            from item in initialBarycenterOrder
            group item.Item3 by new { item.Item1, item.Item2 }
            into grouped
            select grouped;


        while (keepRunning)
        {
            ulong score = 0, best = ulong.MaxValue;
            Node[] order;


            order = initialMedianOrder.Select(x => x.Item3).ToArray();
            score = SolutionHelperFunctions.CrossingCounter(order, G);
            if (score < best)
            {
                foundOrder = order;
                best = score;
            }


            order = initialBarycenterOrder.Select(x => x.Item3).ToArray();
            score = SolutionHelperFunctions.CrossingCounter(order, G);
            if (score < best)
            {
                foundOrder = order;
                best = score;
            }


            order = Optimize<IEnumerable<Node>>(groupedBarycenterOrder, G, OptimizationHelperFunctions.Random);
            score = SolutionHelperFunctions.CrossingCounter(order, G);
            if (score < best)
            {
                foundOrder = order;
                best = score;
            }

            order = Optimize<IEnumerable<Node>>(groupedBarycenterOrder, G, OptimizationHelperFunctions.Reverse);
            score = SolutionHelperFunctions.CrossingCounter(order, G);
            if (score < best)
            {
                foundOrder = order;
                best = score;
            }


            // Disable reverse optimal due to performance issues on calculating the permutations.
            // order = Optimize(initialBarycenterOrder, G, OptimizationHelperFunctions.ReverseOptimal);
            // score = SolutionHelperFunctions.CrossingCounter(order, G);
            // if (score < best)
            // {
            //     foundOrder =  initialBarycenterOrder.Select(x => x.Item3).ToArray();
            //     best = score;
            // }


            order = Optimize<IEnumerable<Node>>(groupedMedianOrder, G,
                OptimizationHelperFunctions.ReverseBarycenter);
            score = SolutionHelperFunctions.CrossingCounter(order, G);
            if (score < best)
            {
                foundOrder = order;
                best = score;
            }

            break;
        }
        if (foundOrder.Length != G.U)
            throw new ArgumentException("The result is not of the correct length: " + foundOrder.Length +
                                        " instead of " + G.U + ".");
        SolutionHelperFunctions.OutputOrderConsole(foundOrder);
    }


    public static Node[] Optimize<T>(IEnumerable<IEnumerable<Node>> groupedList, Graph G,
        Func<IEnumerable<IEnumerable<Node>>, Graph, Node[]> f)
    {
        /*
            Now we want to apply different tie-breaking methods.
            For now, we want:
                - Median ties:
                    * Reverse Barycenter
                - Barycenter ties:
                    * Reverse
                    * Random
                    * Reverse optimal (Dropped due to time/performance--issues

         */
        return f(groupedList, G);
    }
}

public class PACEParser
{
    public static Graph PACEParse(string fileName)
    {
        // Parser for dataformat as read in console, based on https://pacechallenge.org/2024/io/.
        // For detailed information, please refer to the link above.
        Graph G;
        List<string> edges = new List<string>();
        StreamReader reader = new StreamReader(fileName);
        string s;
        int n0 = 0, n1 = 0, m = 0;
        while ((s = reader.ReadLine()) != null)
        {
            if (!s.StartsWith("\n") && !s.StartsWith("c") && s.StartsWith("p"))
            {
                string[] args = s.Split(' ');
                n0 = int.Parse(args[2]);
                n1 = int.Parse(args[3]);
                m = int.Parse(args[4]);
            }
            else
            {
                edges.Add(s);
            }
        }

        if (edges.Count != m)
            throw new ArgumentException("Error: edge list not correct length.");
        G = new Graph(n0, n1, edges.ToArray());
        return G;
    }

    public static Graph PACEConsoleParse()
    {
        StreamReader reader = new StreamReader(Console.OpenStandardInput());
        Graph G;
        List<string> edges = new List<string>();
        string s;
        int n0 = 0, n1 = 0, m = 1;
        while ((s = reader.ReadLine()) != null && edges.Count < m)
        {
            if (!s.StartsWith("\n") && !s.StartsWith("c") && s.StartsWith("p"))
            {
                string[] args = s.Split(' ');
                n0 = int.Parse(args[2]);
                n1 = int.Parse(args[3]);
                m = int.Parse(args[4]);
            }
            else
            {
                edges.Add(s);
            }
        }
        if (edges.Count != m)
            throw new ArgumentException("c Error: edge list not correct length.");
        G = new Graph(n0, n1, edges.ToArray());
        return G;
    }
}

public class Graph
{
    public List<Node>[] NeighboursU, NeighboursL;
    public Node[] Nodes;
    public Edge[] E;
    public int L, U;

    public Graph(int iL, int iU, string[] sE)
    {
        L = iL;
        U = iU;
        Nodes = new Node[iL + iU];
        for (int i = 0; i < iL + iU; i++)
            Nodes[i] = new Node((i + 1).ToString());
        E = new Edge[sE.Length];
        for (int i = 0; i < sE.Length; i++)
            E[i] = new Edge(sE[i]);

        // List with neighbours per node v in L.
        NeighboursL = new List<Node>[L];
        // List with neighbours per node u in U.
        NeighboursU = new List<Node>[U];
        for (int i = 0; i < U; i++)
            NeighboursU[i] = new List<Node>();
        for (int i = 0; i < L; i++)
            NeighboursL[i] = new List<Node>();

        // Each edge (u,v) adds one neighbour for v \in U.
        for (int i = 0; i < E.Length; i++)
        {
            Edge e = E[i];
            NeighboursU[e.Second().order - L - 1].Add(e.First());
            NeighboursL[e.First().order - 1].Add(e.Second());
        }
    }

    public int Size()
    {
        return E.Length;
    }
}

public class Edge
{
    public (Node, Node) edge;

    public Node First()
    {
        return edge.Item1;
    }

    public Node Second()
    {
        return edge.Item2;
    }

    public Edge(string s)
    {
        string[] ss = s.Split(' ');
        string u = ss[0];
        string v = ss[1];
        edge = (new Node(u), new Node(v));
    }
}

public class Node : IEquatable<Node>
{
    public int order;

    public Node(string s)
    {
        try
        {
            order = int.Parse(s);
        }
        catch (Exception e)
        {
            throw new ArgumentException("Exception when parsing name to int:" + e.ToString());
        }
    }

    public override string ToString()
    {
        return order.ToString();
    }

    public static bool operator <(Node a, Node b) => a.order < b.order;
    public static bool operator >(Node a, Node b) => a.order < b.order;

    public static bool operator ==(Node a, Node b)
    {
        if (ReferenceEquals(a, null))
            return false;
        if (ReferenceEquals(b, null))
            return false;
        return a.Equals(b);
    }

    public static bool operator !=(Node a, Node b) => !(a == b);
    public static bool operator <=(Node a, Node b) => a.order != b.order;
    public static bool operator >=(Node a, Node b) => a.order != b.order;

    public bool Equals(Node other)
    {
        if (other == null)
            return false;
        if (order == other.order)
            return true;
        return false;
    }

    public override bool Equals(object obj) => Equals(obj as Node);
    public override int GetHashCode() => order.GetHashCode();
}

public class MedianMethod
{
    // Implementation of the Median Heuristic method.
    public static IEnumerable<Tuple<int, int, Node>> MedianHeuristicMethod(Graph G)
    {
        /*
         * Plan:
         * Datastructures for:
         *   - Nodes
         *   - Side of nodes
         *   - Edges (dictionary stored per node? like Dictionary<Node, Neighbours>)
         *
         *  Assumption structure:
         *      - Nodes: simple integers seperated by spaces (i.e. 1 2 3 4 5 )
         *      - Edges: tuples of pairs of valid nodes seperated by spaces WITHOUT spaces inside the tuples (i.e. (1,2) (2,3)
         *          also: assume first is in L, second in U.
         *
         *
         *  Implementation:
         *  Given Graph G = (L, U, E), with fixed order sigma_L on L, find a order for U such that crossings is minimal.
         *  Median is given by:
         *    for node u in U:
         *      N_u = { v_1, v_2, ..., v_k} where v_i in L neighbours of u (i.e. (u,v_i) in E)
         *        (note: assume v_i are ordered as in sigma_L (i.e. if i < j then sigma_L(v_i) < sigma_L(v_j) )
         *        (also: x_0(v) is the function that returns the x-coordinate of v given the ordering)
         *      med(u) = x_0(v_{floor(k)})
         */


        // Using the neighbours of a node u, the median is the middle of all neighbours of u (under the assumption that
        // the neighbours are sorted. This is a valid assumption given the structure of the edges (they are given in
        // ascending order.))
        // If no neighbours, the median is node '-1'. If two have the same median, they are sorted in arbitrary order
        // based on degree (odd before even). 
        Tuple<int, int, Node>[] Medians = new Tuple<int, int, Node>[G.U];

        for (int i = 0; i < G.U; i++)
        {
            int sz = G.NeighboursU[i].Count;
            if (sz > 1)
            {
                Medians[i] =
                    new Tuple<int, int, Node>(G.NeighboursU[i][(int)Math.Floor((double)sz / 2) - 1].order, -sz % 2,
                        G.Nodes[G.L + i]);
            }
            else
            {
                Medians[i] =
                    new Tuple<int, int, Node>(sz == 0 ? -1 : 0, -sz % 2,
                        G.Nodes[G.L + i]);
            }
        }

        // The medians are sorted, as mentioned above, in order of median, degree and finally the node itself
        // (default node order as given at the start, 0 to |L + |U|) in ascending order.
        IEnumerable<Tuple<int, int, Node>> sortedMedians =
            Medians.OrderBy(y => y.Item1)
                .ThenBy(y => y.Item2)
                .ThenBy(y => y.Item3.order);

        return sortedMedians;
    }
}

public class BarycenterMethod
{
    public static IEnumerable<Tuple<double, int, Node>> BarycenterHeuristicMethod(Graph G)
    {
        /*
         * Similar to median, except the assigned position is the average order of its neighbours,
         * rather than the order of the middle neighbour.
         */


        // Using the neighbours of a node u, the median is the average order of all neighbours of u.

        // I have not found what to do if no neighbours or if two have the same barycenter.
        // Currently, the datastructure expects a tuple, so I either rewrite, or think of something to fill a tuple.

        // If no neighbours, the median is node '-1'. If two have the same barycenter, they are kept in the same order
        // they started (node order between |L| and |L| + |U|).
        Tuple<double, int, Node>[] Barycenters = new Tuple<double, int, Node>[G.U];

        for (int i = 0; i < G.U; i++)
        {
            List<Node> nbs = G.NeighboursU[i];
            int sz = G.NeighboursU[i].Count;
            Barycenters[i] =
                new Tuple<double, int, Node>(sz == 0 ? 0.0 : nbs.Average(x => x.order), 0, G.Nodes[G.L + i]);
        }

        // The medians are sorted, as mentioned above, in order of median, degree and finally the node itself
        // (default node order as given at the start, 0 to |L + |U|) in ascending order.
        IEnumerable<Tuple<double, int, Node>> SortedBarycenters =
            Barycenters.OrderBy(y => y.Item1)
                .ThenBy(y => y.Item2)
                .ThenBy(y => y.Item3.order);
        
        return SortedBarycenters;
    }
}

// This class contains helperfunctions dedicated to mostly debugging:
// E.g. crossingscalculation, output to console, compare with existing solutionfile, etc.

public class SolutionHelperFunctions
{
    // Method to calculate number of crossings of a given order, using standard formula.
    // (for each edge (u,v), sum all edges with 'start' left of u, and 'end' right of v) 
    public static ulong CrossingCounter(Node[] foundOrder, Graph G)
    {
        ulong numberOfCrossings = 0;

        // Attempt at more optimal implementation:
        // Since the neighbours are sorted, it is enough when we find the first that satisfies the condition,
        // and all the remaining nodes (with a larger order) must satisfy the condition  as well.
        // (assuming, of course, iteration from left to right)
        List<Node> N_u, N_uprime;
        int l, u, result;
        for (int i = 0; i < foundOrder.Length; i++) // u
        {
            N_u = G.NeighboursU[foundOrder[i].order - G.L - 1];
            for (int k = i + 1; k < foundOrder.Length; k++) // u'
            {
                N_uprime = G.NeighboursU[foundOrder[k].order - G.L - 1];
                l = 1;
                u = N_uprime.Count;
                for (int j = 0; j < N_u.Count(); j++) // v
                {
                    result = BinaryCount(l, u,
                        N_u[j], // v
                        N_uprime); // all v'
                    l = result == 0 ? 1 : result;

                    numberOfCrossings += (ulong)result;
                }
            }
        }

        return numberOfCrossings;
    }

    // Method to write found order to a specified file.
    // Mostly debugging, and in order to (possibly) make own testcases.
    public static void OutputOrderFile(Node[] foundOrder, string outputFile)
    {
        File.WriteAllLines(outputFile, foundOrder.Select(x => x.ToString()));
    }

    // Small method that write the found order to console.
    // Mostly debugging purposes, to see if found order is indeed correct.
    public static void OutputOrderConsole(Node[] foundOrder)
    {
        for (int i = 0; i < foundOrder.Length - 1; i++)
            Console.WriteLine(foundOrder[i].order);
        Console.Write(foundOrder[foundOrder.Length - 1].order);
    }

    // Way to count number of crossings by using a form of binary search:
    // We know that the neighbours of a node are sorted in ascending order, hence,
    // the moment we find the first node that satisfy the crossingscondition
    // (default definition: (u,v). (u', v') cross <=> u.order < u'.order && v.order > v'.order
    // (or vice versa, but we need to check only one, otherwise we count crossings double)
    // So, since nodes are sorted in ascending order, if we find u' \in N_v' crosses with (u,v), then all nodes before/after u' too.
    // Conclusion: find first element that is bigger then the input node, and we can return the crossingscount
    // by returning l or u (already compensated for the 0-indexbased-ness).
    // (if we chose u' > threshold, we would get |N_v'| - indexOf(u') )
    public static int BinaryCount(int l, int u, Node threshold, List<Node> order)
    {
        // l is lower bound, u upper bound, threshold the value to check against.
        // If segment length is one, it either matches or not. Determine result based on that.
        if (l == u)
            return order[l - 1].order < threshold.order ? l : 0;
        if (u - l == 1)
            return order[l - 1].order < threshold.order ? order[u - 1].order < threshold.order ? u : l : 0;

        // Take midpoint of segment, and check if it is larger of smaller then the threshold.
        // If m is larger (or equal), the smallest index (if it exists) that satisfies is in the left half of the remaining segment.
        // If m is smaller, then either it is the first, or the largest is in the right half.
        int m = (l + u) / 2;

        if (order[m].order < threshold.order)
            return BinaryCount(m, u, threshold, order);
        //if (order[m].order == threshold.order)
        //    return BinaryCount(l,m, threshold, order);
        return BinaryCount(l, m, threshold, order);
    }
}

// This class contains helperfunctions that are used for optimizing the found solution, and their dependencies.
// These functions are applied after the general method is tried, and return their best result for that specific method.

public static class OptimizationHelperFunctions
{
    // Two helperfunctions to (easier) retrieve barycenter/median of a node.
    public static double GetBarycenter(Node x, Graph G)
    {
        return G.NeighboursU[x.order - G.L - 1].DefaultIfEmpty(new Node("-1")).Average(y => y.order);
    }
    

    // -----
    // Helperfunctions for applying different tie-breaking implementations.

    public static Node[] ReverseBarycenter(this IEnumerable<IEnumerable<Node>> groupedList, Graph G)
    {
        IEnumerable<Node> result = Enumerable.Empty<Node>();
        foreach (IEnumerable<Node> order in groupedList)
        {
            IEnumerable<Node> temp = order.OrderByDescending(x => GetBarycenter(x, G));
            result = result.Concat(temp);
        }

        if (result.Count() != G.U)
            throw new ArgumentException($"The result is not of the correct length: {result.Count()} instead of {G.U}.");

        return result.ToArray();
    }

    public static Node[] Reverse(this IEnumerable<IEnumerable<Node>> groupedList, Graph G)
    {
        IEnumerable<Node> result = Enumerable.Empty<Node>();
        foreach (IEnumerable<Node> order in groupedList)
            result = result.Concat(order.Reverse());

        if (result.Count() != G.U)
            throw new ArgumentException($"The result is not of the correct length: {result.Count()} instead of {G.U}.");

        return result.ToArray();
    }

    public static Node[] Random(IEnumerable<IEnumerable<Node>> groupedList, Graph G)
    {
        // Apply a method called Fisher-Yates shuffle.
        // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
        // (Basically just swapping each elements with another, at 'random')

        Random random = new Random();
        IEnumerable<Node> result = Enumerable.Empty<Node>();
        foreach (IEnumerable<Node> order in groupedList)
        {
            Node[] elements = order.ToArray();
            for (int i = order.Count() - 1; i > 0; i--)
            {
                int swapIndex = random.Next(i + 1);
                (elements[i], elements[swapIndex]) = (elements[swapIndex], elements[i]);
            }

            result = result.Concat(elements);
        }

        if (result.Count() != G.U)
            throw new ArgumentException($"The result is not of the correct length: {result.Count()} instead of {G.U}.");

        return result.ToArray();
    }
}