:: (batch) Script to run the solver on all testcases at the specified location.
@echo off
@cd "Implementation"
@SET "f=%~dp0%IOFiles"
@SET "p=%f%\OutputFiles"
@FOR /r "%f%\InputFiles" %%i in (*.gr) DO (
	echo Next problem, %%~ni:
	dotnet run "%%i" "%p%\%%~ni.ans" --property WarningLevel=0	
	echo[
)
PAUSE