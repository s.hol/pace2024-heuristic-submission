## Disclaimer

The submission to the PACE challenge is part of a bachelor thesis (Department of Mathematics, University Utrecht). We made quite a few additions that do not impact the *solver* itself, but more quality of life (see section **Additional features**). We did decide to somewhat trim the submission to OPTIL.io (single file, remove WIP and QoL code, etc.) and modified to *only* use console, rather then a check if there are arguments, due to issues with parameters from OPTIL.io resulting in `IndexOutOfRangeException`.

For transparency, the actual submitted file is included in the repository as well, called `submittedFile.cs`.

If any of this is a problem, please let us know and we will refactor the repository.


## Brief description of the submission

The solver itself is very linear: run five methods and choose the result with the minimal number of crossings.

The five methods used are the barycenter and median methods, and three different tiebreaking heuristics. For any remaining ties, the original order is preserved.

The exact methods are:
- For the barycenter heuristic:
    - Standard: sort on barycenter;
    - reverse: reverse tied vertices w.r.t. the original order;
    - random: a random permutation of the tied vertices.
- For the median heuristic:
    - Standard: sort on median. In case of ties, odd before even;
    - Reverse barycenter: sort tied vertices on barycenter, then reverse.
    NOTE: We implemented this as "sort descending on barycenter", and only after submitting realised that this is ambigious. It should, most likely, have been: sort on barycenter and *tied vertices w.r.t. the barycenter method* should be reversed, rather then *all vertices*.



## How to build and run the solver

Note that this is a C# (.NET 8.0) application.

First, the program has two (optional) parameters for the inputfile (``path/to/problem/problem.gr``) and outputfile (``path/to/solution/solution.ans``). If none are given, the solver uses ``stdin`` and ``stdout``.

There are several options to run the solver. The following all assume that `dotnet` is an available command:

- Running `dotnet build/run`: default building/running method for dotnet projects. Run this is `./Implementation`, containing the `.sln`.

- Running `Manualsolver.sh` or `.bat` (functional identical): script that runs the solver on all test cases in `IOFiles/InputFiles`. This serves as the inputfilepath paramter. It defines the outputfile-parameter with the same name as the problem name, i.e. `1.gr` becomes `1.ans`. The output is located at `IOFiles/OutputFiles`, which combined gives the outputfilepath. It then calls `dotnet run` with those parameters. Existing files are overwritten.
NOTE: the problem files are not included in to repository by default, so using this requires inserting the `.gr` in `IOFiles/InputFiles`.

- Calling `solver.sh` or `.bat` (functional identical):  A short script that passes parameters along using `dotnet run`. Runs only one instance. This is the advised method if you intend to use ``pace2024tester`` or `pace2024verifier`. Again, if no paramters are passed along, the solver uses `stdin` and `stdout`.

If the `dotnet` command is not available, to run the solver please build and run the project `Implementation.sln` with your tool of choice.




One thing to note: we ran into some issues with shell scripts on GitLab, where the encoding of the files change. It should be remedied using `.gitattributes`, but still, when running the program using the `.sh` scripts, please keep this in mind.

### Libraries needed

Besides a compiler compatible with C# and .Net8.0 and some usings (default C# usings), there are no external libraries used.


## Additional features

This submission is part of a bachelor thesis, and as such there are several features that were added during the project that are not really needed for the submission, but more for convenience as well as some data gathering purposes (used in the thesis). In the end, we still decided to include them, for completeness' sake. Please let us know if that is a problem.

This includes:
- Several bath scripts for debugging (outputting densities, comparing crossingcount results with `pace2024verifier`, etc.);
- A .csv file with data comparing the different methods used;
- A graph generator to generate own testcases;
- A test suite used to test properties of testcases (e.g. sorted neighbours, edges, etc.);
- A (barebones) logging system to be able to print values without it registring as "output".

A more detailed overview will be included in the solver description.